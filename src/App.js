import React, { Component } from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import ReactFlagsSelect from "react-flags-select";
import axios from "axios";
import ConfirmedCard from "./Components/ConfirmCard";
import DeathsCard from "./Components/DeathsCard";
import Recovered from "./Components/Recovered";
import DevelopedBy from "./Components/DevelopedBy";
import Chart from "./Components/Chart";

export class App extends Component {
  state = {
    err: null,
    confirmed: null,
    deaths: null,
    recovered: null,
    country: null,
    countryCode: null,
    historyData: null
  };

  componentWillMount() {
    this.getCountry();
  }

  getCountry = () => {
    axios
      .get(`https://get.geojs.io/v1/ip/geo.json`)
      .then(res => {
        this.getLetestData(res.data.country_code3);
        this.setState({ country: res.data.country });
        this.setState({ countryCode: res.data.country_code });
      })
      .catch(err => {
        this.setState({ err });
      });
  };

  onSelectFlag = countryCode => {
    this.setState({ country: null });
    this.setState({ confirmed: null });
    this.setState({ deaths: null });
    this.setState({ recovered: null });
    this.setState({ historyData: null });

    axios
      .get(`https://restcountries.eu/rest/v2/alpha/${countryCode}`)
      .then(res => {
        setTimeout(() => {
          this.getLetestData(res.data.alpha3Code);
          this.setState({ country: res.data.name });
          this.setState({ countryCode: res.data.alpha2Code });
        }, 1000);
      })
      .catch(() => {
        this.setState({ err: "Country data not found" });
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      });
  };

  getLetestData = country => {
    axios
      .get(`https://covidapi.info/api/v1/country/${country}/latest`)
      .then(res => {
        this.getCountryHistoryData(country);
        let data = res.data.result;
        for (let key in data) {
          if (data.hasOwnProperty(key)) {
            this.setState({ confirmed: data[key].confirmed });
            this.setState({ deaths: data[key].deaths });
            this.setState({ recovered: data[key].recovered });
          }
        }
      })
      .catch(() => {
        this.setState({ err: "Country data not found" });
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      });
  };

  getCountryHistoryData = country => {
    axios
      .get(`https://covidapi.info/api/v1/country/${country}`)
      .then(res => {
        this.setState({ historyData: res.data.result });
      })
      .catch(() => {
        this.setState({ err: "Country data not found" });
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      });
  };

  render() {
    return (
      <div>
        {
          <nav className="navbar navbar-dark bg-dark">
            <div className="container">
              <span className="navbar-brand">
                COVID-19 Report
                <br />
                <b>
                  {this.state.country || (
                    <SkeletonTheme color="#202020" highlightColor="#444">
                      <Skeleton height={20} />
                    </SkeletonTheme>
                  )}
                </b>
              </span>
              {this.state.countryCode ? (
                <ReactFlagsSelect
                  onSelect={this.onSelectFlag}
                  searchable={true}
                  showSelectedLabel={false}
                  defaultCountry={this.state.countryCode}
                />
              ) : (
                <SkeletonTheme color="#202020" highlightColor="#444">
                  <Skeleton height={20} />
                </SkeletonTheme>
              )}
            </div>
          </nav>
        }
        <div className="container mt-3">
          {this.state.err ? (
            <div class="alert alert-danger" role="alert">
              {this.state.err}
            </div>
          ) : null}
          <div className="row">
            <div className="col-sm mb-3">
              {this.state.confirmed != null ? (
                <ConfirmedCard number={this.state.confirmed} />
              ) : (
                <Skeleton height={150} />
              )}
            </div>
            <div className="col-sm mb-3">
              {this.state.deaths != null ? (
                <DeathsCard number={this.state.deaths} />
              ) : (
                <Skeleton height={150} />
              )}
            </div>
            <div className="col-sm mb-3">
              {this.state.recovered != null ? (
                <Recovered number={this.state.recovered} />
              ) : (
                <Skeleton height={150} />
              )}
            </div>
          </div>
          <div className="row">
            <div className="col-sm">
              {this.state.historyData != null ? (
                <Chart data={this.state.historyData} />
              ) : (
                <Skeleton height={250} width="100%" />
              )}
            </div>
          </div>
          <div className="row mt-5 mb-5">
            <div className="col-sm">
              <DevelopedBy />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
