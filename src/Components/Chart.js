import React from "react";
import {
  ResponsiveContainer,
  AreaChart,
  Area,
  XAxis,
  YAxis,
  Legend,
  CartesianGrid,
  Tooltip
} from "recharts";

function Chart(props) {
  let chartData = [];
  const propData = props.data;
  const objToArray = Object.keys(propData).map(o => {
    propData[o].date = o;
    return [propData[o]];
  });

  for (var i = 0; i < objToArray.length; i++) {
    for (var z = 0; z < objToArray[i].length; z++) {
      chartData.push(objToArray[i][z]);
    }
  }

  return (
    <ResponsiveContainer width="100%" height={300}>
      <AreaChart data={chartData}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="date" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Area
          type="monotone"
          dataKey="confirmed"
          stroke="#ffaa00"
          fill="#ffc243"
        />

        <Area
          type="monotone"
          dataKey="recovered"
          stroke="#08ff00"
          fill="#83dd80"
        />

        <Area
          type="monotone"
          dataKey="deaths"
          stroke="#ff000c"
          fill="#ff848a"
        />
      </AreaChart>
    </ResponsiveContainer>
  );
}

export default Chart;
