import React from 'react'

function DataCard(props) {
    return (
        <div className=" card text-white bg-warning">
            <div className="card-body">
                <h5 className="card-title">Confirmed</h5>
                <h1 className="card-text"><strong>{props.number}</strong></h1>
            </div>
        </div>
    )
}

export default DataCard
