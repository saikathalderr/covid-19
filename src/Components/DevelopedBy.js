import React from "react";

function DevelopedBy() {
  return (
    <div className="text-center developedBy">
      Developed By{" "}
      <b>
        <a href="https://www.linkedin.com/in/saikathalderr">Saikat Halder</a>
      </b>
    </div>
  );
}

export default DevelopedBy;
