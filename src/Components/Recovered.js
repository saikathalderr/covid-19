import React from 'react'

function Recoverd(props) {
    return (
        <div className=" card text-white bg-success">
            <div className="card-body">
                <h5 className="card-title">Recovered</h5>
                <h1 className="card-text"><strong>{props.number}</strong></h1>
            </div>
        </div>
    )
}

export default Recoverd
