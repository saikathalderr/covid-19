import React from 'react'

function DeathsCard(props) {
    return (
        <div className="card text-white bg-danger">
            <div className="card-body">
                <h5 className="card-title">Deaths</h5>
                <h1 className="card-text"><strong>{props.number}</strong></h1>
            </div>
        </div>
    )
}

export default DeathsCard
